package com.example.gitlist.feature.auth;

import android.content.Context;

import com.example.gitlist.core.BaseModel;
import com.example.gitlist.network.AccessTokenResponse;
import com.example.gitlist.network.AuthTokenRequest;

import io.reactivex.Single;

/**
 * Created by anand on 2019-06-23 at 17:33.
 */
public class UserInfoModel extends BaseModel {

    public UserInfoModel(Context context) {
        super(context);
    }

    Single<AccessTokenResponse> getAuthToken(String code) {
        return networkManager.getUserTokenApi()
                .getAccessToken(new AuthTokenRequest(code));
    }

}

package com.example.gitlist.feature.userlisting;

import android.widget.Filter;

import com.example.gitlist.feature.adapter.UserListingAdapter;
import com.example.gitlist.model.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ananddubey
 * Dated- 2019-06-18
 */
public class UserSearchFilter extends Filter {

    private UserListingAdapter adapter;

    private List<User> filteredData = new ArrayList<>();
    private List<User> originalData = new ArrayList<>();

    public UserSearchFilter(UserListingAdapter adapter) {
        this.adapter = adapter;
        originalData.clear();
        originalData.addAll(adapter.getData());
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        filteredData.clear();
        if (constraint.length() == 0) {
            adapter.filterApplied = false;
            filteredData.addAll(originalData);
        } else {
            adapter.filterApplied = true;
            final String query = constraint.toString().toLowerCase().trim();
            for (final User user : originalData) {
                String name = user.userName;
                String trimmed = name.replace(" ", "");
                if (trimmed.startsWith(query) || name.toLowerCase().startsWith(query))
                    filteredData.add(user);
            }
        }
        final FilterResults results = new FilterResults();
        results.values = filteredData;
        results.count = filteredData.size();
        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
        Object value = results.values;
        if (value instanceof ArrayList)
            adapter.swapAll((ArrayList<User>) value);
    }

    public void updateOriginalData(List<User> users) {
        originalData.addAll(users);
    }

    public void invalidateData() {
        originalData.clear();
        filteredData.clear();
    }
}

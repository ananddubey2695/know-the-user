package com.example.gitlist.feature.userlisting;

import android.content.Context;

import com.example.gitlist.core.BaseModel;
import com.example.gitlist.model.User;

import java.util.List;

import io.reactivex.Single;

/**
 * Created by ananddubey
 * Dated- 2019-06-18
 */
public class UserListingModel extends BaseModel {

    public UserListingModel(Context context) {
        super(context);
    }

    // try fetching list from dB id list is empty then fetch list from GIT API
    Single<List<User>> getUserList(Integer since) {
        if (since == null)
            return databaseManager.userDao()
                    .getUsers()
                    .flatMap(users -> {
                        if (users == null || users.isEmpty())
                            return fetchUserList(null);
                        else return Single.just(users);
                    });
        else return fetchUserList(since);
    }

    // fetch user list from GIT API and add to dB
    private Single<List<User>> fetchUserList(Integer since) {
        return networkManager.getGitApi()
                .getScans(since)
                .doOnSuccess(users -> databaseManager.userDao()
                        .saveUsers(users));
    }

    // fetch user list from GIT API and add to dB
    public Single<List<User>> searchUserByName(String name) {
        return networkManager.getGitApi()
                .getUserByName(name)
                .flatMap(userSearchResponse -> Single.just(userSearchResponse.users));
    }
}

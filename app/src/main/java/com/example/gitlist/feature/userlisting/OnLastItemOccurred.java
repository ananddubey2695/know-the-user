package com.example.gitlist.feature.userlisting;

/**
 * Created by ananddubey
 * Dated- 2019-06-18
 */
public interface OnLastItemOccurred {

    void lastItemShown();
}

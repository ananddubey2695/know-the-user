package com.example.gitlist.feature.auth;

import android.content.Context;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.gitlist.core.BaseViewModel;
import com.example.gitlist.network.AccessTokenResponse;

import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by anand on 2019-06-23 at 17:32.
 */
public class UserInfoViewModel extends BaseViewModel {

    private static final String TAG = "UserInfoViewModel";
    private UserInfoModel model;

    private MutableLiveData<String> accessTokenLiveData;

    UserInfoViewModel(Context context) {
        super(context);
        model = new UserInfoModel(context);
    }

    void getAccessToken(String code) {
        model.getAuthToken(code)
                .subscribeOn(Schedulers.io())
                .subscribe(new SingleObserver<AccessTokenResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onSuccess(AccessTokenResponse accessTokenResponse) {
                        accessTokenLiveData.postValue(accessTokenResponse.accessToken);
                        Log.e(TAG, "onSuccess: ACCESS TOKEN" + accessTokenResponse);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "onError: ", e);
                    }
                });
    }

    public LiveData<String> getAccessTokenLiveData() {
        if (accessTokenLiveData == null)
            accessTokenLiveData = new MutableLiveData<>();
        return accessTokenLiveData;
    }
}

package com.example.gitlist.feature.userlisting;

import android.content.Context;

import androidx.lifecycle.MutableLiveData;

import com.example.gitlist.core.BaseViewModel;
import com.example.gitlist.model.User;

import java.util.List;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by ananddubey
 * Dated- 2019-06-18
 */
public class UserListingViewModel extends BaseViewModel {

    private UserListingModel model;

    private MutableLiveData<List<User>> usersLiveData;

    private MutableLiveData<List<User>> searchResultsLiveData;

    public UserListingViewModel(Context context) {
        super(context);
        model = new UserListingModel(context);
    }

    void fetchUsers(Integer startId) {
        model.getUserList(startId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<List<User>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onSuccess(List<User> users) {
                        usersLiveData.postValue(users);
                    }

                    @Override
                    public void onError(Throwable e) {
                        usersLiveData.postValue(null);
                    }
                });
    }

    void searchUserByName(String name) {
        model.searchUserByName(name)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<List<User>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onSuccess(List<User> users) {
                        searchResultsLiveData.postValue(users);
                    }

                    @Override
                    public void onError(Throwable e) {
                        searchResultsLiveData.postValue(null);
                    }
                });
    }

    MutableLiveData<List<User>> getUsersLiveData() {
        if (usersLiveData == null)
            usersLiveData = new MutableLiveData<>();
        return usersLiveData;
    }

    public MutableLiveData<List<User>> getSearchResultsLiveData() {
        if (searchResultsLiveData == null)
            searchResultsLiveData = new MutableLiveData<>();
        return searchResultsLiveData;
    }
}

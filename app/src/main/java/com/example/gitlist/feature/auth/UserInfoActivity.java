package com.example.gitlist.feature.auth;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatTextView;

import com.example.gitlist.R;
import com.example.gitlist.feature.userlisting.UserListingActivity;

/**
 * Created by anand on 2019-06-23 at 17:28.
 */
public class UserInfoActivity extends AppCompatActivity {

    private static final String TAG = "UserInfoActivity";
    UserInfoViewModel viewModel;
    AppCompatTextView mToken;
    AppCompatButton mListingButton;
    String accessToken;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_info);

        mToken = findViewById(R.id.user_token);
        mListingButton = findViewById(R.id.listing_button);

        Intent intent = getIntent();
        String uri = "";
        if (intent.getData() != null)
            uri = intent.getData().getQueryParameter("code");

        Log.e(TAG, "onCreate: CODE " + uri);

        viewModel().getAccessTokenLiveData()
                .observe(this, token -> {
                    if (!token.isEmpty()) {
                        mToken.setText(token);
                        accessToken = token;
                    }
                    mListingButton.setVisibility(View.VISIBLE);

                });

        if (uri != null && !uri.isEmpty())
            viewModel().getAccessToken(uri);

        mListingButton.setOnClickListener(view ->
                startActivity(UserListingActivity.
                        getIntent(UserInfoActivity.this, accessToken)));
    }


    public UserInfoViewModel viewModel() {
        if (viewModel == null)
            viewModel = new UserInfoViewModel(getApplicationContext());
        return viewModel;
    }
}

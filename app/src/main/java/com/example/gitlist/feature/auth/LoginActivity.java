package com.example.gitlist.feature.auth;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;

import com.example.gitlist.R;

/**
 * Created by anand on 2019-06-23 at 16:47.
 */
public class LoginActivity extends AppCompatActivity {

    AppCompatButton mLoginButton;
    AppCompatEditText mUserName;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mUserName = findViewById(R.id.user_token);
        mLoginButton = findViewById(R.id.login_button);
        mLoginButton.setOnClickListener(view -> {
            if (mUserName != null && mUserName.getText() != null
                    && !mUserName.getText().toString().isEmpty()) {

                Intent intent = new Intent(Intent.ACTION_VIEW);
                String uri = "https://github.com/login/oauth/authorize?client_id=" +
                        getString(R.string.client_id) + "&redirect_uri=https://knowtheuser.com";
                uri += "&login=" + mUserName.getText().toString();
                intent.setData(Uri.parse(uri));
                startActivity(intent);
            } else
                Toast.makeText(this, "Please enter a valid username", Toast.LENGTH_SHORT).show();
        });
    }
}

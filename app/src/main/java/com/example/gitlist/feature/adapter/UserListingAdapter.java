package com.example.gitlist.feature.adapter;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.gitlist.R;
import com.example.gitlist.feature.userlisting.OnLastItemOccurred;
import com.example.gitlist.feature.userlisting.UserSearchFilter;
import com.example.gitlist.model.User;
import com.facebook.drawee.view.SimpleDraweeView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ananddubey
 * Dated- 2019-06-18
 */
public class UserListingAdapter extends RecyclerView.Adapter<UserListingAdapter.UserViewHolder>
        implements Filterable, TextWatcher {

    private List<User> userList = new ArrayList<>();
    private OnLastItemOccurred lastItemOccurred;
    private UserSearchFilter filter;

    public boolean filterApplied = false;
    public boolean searchApplied = false;

    public UserListingAdapter(List<User> users, OnLastItemOccurred lastItemOccurred) {
        if (!userList.isEmpty())
            userList.clear();
        userList.addAll(users);
        this.lastItemOccurred = lastItemOccurred;
        filter = new UserSearchFilter(this);
    }

    @NonNull
    @Override
    public UserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        return new UserViewHolder(inflater.inflate(R.layout.item_list_user,
                parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull UserViewHolder holder, int position) {
        holder.updateUserData(userList.get(position));
        if (position == getItemCount() - 1 && !filterApplied && !searchApplied && lastItemOccurred != null)
            lastItemOccurred.lastItemShown();
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    public Integer getLastProductId() {
        return userList.get(this.getItemCount() - 1).id;
    }

    public List<User> getData() {
        return userList;
    }

    public void updateData(List<User> users) {
        if (users != null && userList != null) {
            searchApplied = false;
            userList.addAll(users);
            filter.updateOriginalData(users);
            notifyDataSetChanged();
        }
    }

    public void swapAll(List<User> users) {
        if (users != null && !users.isEmpty()) {
            userList.clear();
            userList.addAll(users);
            notifyDataSetChanged();
        }
    }

    public void swapSearchResults(List<User> users) {
        filter.invalidateData();
        filter.updateOriginalData(users);
        filterApplied = false;
        searchApplied = true;
        if (users != null && !users.isEmpty()) {
            userList.clear();
            userList.addAll(users);
            notifyDataSetChanged();
        }
    }

    public void clear() {
        userList.clear();
        filterApplied = false;
        searchApplied = false;
        filter.invalidateData();
        notifyDataSetChanged();
    }

    @Override
    public Filter getFilter() {
        return filter;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        getFilter().filter(s);
    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    static class UserViewHolder extends RecyclerView.ViewHolder {

        AppCompatTextView mUserName;
        SimpleDraweeView mAvatar;

        UserViewHolder(@NonNull View itemView) {
            super(itemView);
            mUserName = itemView.findViewById(R.id.user_token);
            mAvatar = itemView.findViewById(R.id.avatar);
        }

        void updateUserData(User user) {
            if (user != null) {
                if (user.userName != null && !user.userName.isEmpty()) {
                    mUserName.setText(user.userName);
                    mUserName.setVisibility(View.VISIBLE);
                } else
                    mUserName.setVisibility(View.GONE);

                if (user.avatar != null && !user.avatar.isEmpty()) {
                    mAvatar.setImageURI(user.avatar);
                    mUserName.setVisibility(View.VISIBLE);
                } else
                    mAvatar.setVisibility(View.GONE);
            }
        }
    }
}

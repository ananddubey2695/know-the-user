package com.example.gitlist.feature.userlisting;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.gitlist.R;
import com.example.gitlist.feature.adapter.UserListingAdapter;
import com.facebook.drawee.backends.pipeline.Fresco;

import java.util.Collections;

public class UserListingActivity extends AppCompatActivity {

    private static final String TAG = "UserListingActivity";
    private static final String KEY_ACCESSTOKEN = "ACCESSTOKEN";
    RecyclerView mUsers;
    UserListingAdapter adapter;
    UserListingViewModel viewModel;
    AppCompatTextView mNoUserMessage;
    AppCompatEditText mFilterView;
    AppCompatEditText mSearchView;
    AppCompatButton mSearchButton;
    AppCompatButton mClearButton;

    String accessToken = "";

    public static Intent getIntent(Context context, String accessToken) {
        Intent intent = new Intent(context, UserListingActivity.class);
        if (!accessToken.isEmpty())
            intent.putExtra(KEY_ACCESSTOKEN, accessToken);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fresco.initialize(this);
        setContentView(R.layout.activity_user_listing);

        if (getIntent().getStringExtra(KEY_ACCESSTOKEN) != null
                && !getIntent().getStringExtra(KEY_ACCESSTOKEN).isEmpty()) {

            // can use accesstoken for all API's
            accessToken = getIntent().getStringExtra(KEY_ACCESSTOKEN);
        }

        Log.e(TAG, "onCreate: REDIRECT" + getIntent());

        mUsers = findViewById(R.id.user_list);
        mNoUserMessage = findViewById(R.id.no_user_message);
        mFilterView = findViewById(R.id.filter_user);
        mSearchView = findViewById(R.id.search_user);
        mSearchButton = findViewById(R.id.search_button);
        mClearButton = findViewById(R.id.clear_all_button);

        adapter = new UserListingAdapter(Collections.emptyList(), () ->
                viewModel().fetchUsers(adapter.getLastProductId()));

        mUsers.setLayoutManager(new LinearLayoutManager(this));
        mUsers.setHasFixedSize(true);
        mUsers.setAdapter(adapter);

        viewModel().getUsersLiveData()
                .observe(this, users -> {
                    if (users != null && !users.isEmpty()) {
                        adapter.updateData(users);
                        mFilterView.setVisibility(View.VISIBLE);
                        mUsers.setVisibility(View.VISIBLE);
                        mNoUserMessage.setVisibility(View.GONE);
                    } else {
                        mFilterView.setVisibility(View.GONE);
                        mUsers.setVisibility(View.GONE);
                        mNoUserMessage.setVisibility(View.VISIBLE);
                    }
                });

        viewModel().getSearchResultsLiveData()
                .observe(this, users -> {
                    if (users != null && !users.isEmpty()) {
                        adapter.swapSearchResults(users);
                        mFilterView.setVisibility(View.VISIBLE);
                        mUsers.setVisibility(View.VISIBLE);
                        mNoUserMessage.setVisibility(View.GONE);
                    } else {
                        mFilterView.setVisibility(View.GONE);
                        mUsers.setVisibility(View.GONE);
                        mNoUserMessage.setVisibility(View.VISIBLE);
                    }
                });

        viewModel().fetchUsers(null);
        mFilterView.addTextChangedListener(adapter);

        mSearchButton.setOnClickListener(view -> {
            if (mSearchView != null && mSearchView.getText() != null) {
                String textToSearch = mSearchView.getText().toString();

                if (!textToSearch.isEmpty()) {
                    viewModel().searchUserByName(textToSearch);
                    mFilterView.setText("");
                }
            }
        });

        mClearButton.setOnClickListener(view -> {
            adapter.clear();
            mFilterView.setText("");
            mSearchView.setText("");
            viewModel().fetchUsers(null);
        });
    }

    public UserListingViewModel viewModel() {
        if (viewModel == null)
            viewModel = new UserListingViewModel(getApplicationContext());
        return viewModel;
    }
}

package com.example.gitlist.model;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.example.gitlist.core.Core;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ananddubey
 * Dated- 2019-06-18
 */

@Entity(tableName = "user")
public class User extends Core {

    @PrimaryKey
    public Integer id;

    @SerializedName("login")
    public String userName;

    @SerializedName("avatar_url")
    public String avatar;
}

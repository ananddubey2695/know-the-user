package com.example.gitlist.database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.gitlist.model.User;

import java.util.List;

import io.reactivex.Single;

/**
 * Created by ananddubey
 * Dated- 2019-06-19
 */

@Dao
public interface UserDao {

    @Query("SELECT * FROM user")
    Single<List<User>> getUsers();

    @Insert
    void saveUsers(List<User> users);
}

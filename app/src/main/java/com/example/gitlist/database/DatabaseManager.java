package com.example.gitlist.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.gitlist.model.User;

/**
 * Created by ananddubey
 * Dated- 2019-06-19
 */
@Database(entities = {User.class}, version = 1)
public abstract class DatabaseManager extends RoomDatabase {

    private static DatabaseManager instance;

    public abstract UserDao userDao();

    public static DatabaseManager getInstance(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(context.getApplicationContext(), DatabaseManager.class,
                    "users_db")
                    .allowMainThreadQueries()
                    .build();
        }
        return instance;
    }

    public static void invalidate() {
        instance = null;
    }
}

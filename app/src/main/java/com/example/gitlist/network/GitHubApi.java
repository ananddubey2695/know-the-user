package com.example.gitlist.network;

import com.example.gitlist.model.User;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by anand on 2019-05-05 at 13:56.
 */
public interface GitHubApi {

    @GET("users")
    Single<List<User>> getScans(@Query("since") Integer since);

    @GET("search/users")
    Single<UserSearchResponse> getUserByName(@Query("q") String name);

    @POST("login/oauth/access_token")
    @Headers("Accept: application/json")
    Single<AccessTokenResponse> getAccessToken(@Body AuthTokenRequest request);
}

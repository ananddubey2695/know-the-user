package com.example.gitlist.network;

import com.example.gitlist.core.Core;
import com.google.gson.annotations.SerializedName;

/**
 * Created by anand on 2019-06-23 at 17:37.
 */
public class AccessTokenResponse extends Core {

    @SerializedName("token_type")
    String type;

    @SerializedName("access_token")
    public String accessToken;
}

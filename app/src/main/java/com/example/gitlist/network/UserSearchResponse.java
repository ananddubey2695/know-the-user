package com.example.gitlist.network;

import com.example.gitlist.core.Core;
import com.example.gitlist.model.User;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by anand on 2019-06-23 at 11:52.
 */
public class UserSearchResponse extends Core {

    @SerializedName("items")
    public List<User> users;
}

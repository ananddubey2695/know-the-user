package com.example.gitlist.network;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;


public class NetworkManager {

    private static final String BASE_URL = "https://api.github.com";

    private Retrofit retrofit;

    private static NetworkManager instance;

    private OkHttpClient client;
    HttpLoggingInterceptor interceptor;

    private NetworkManager() {
        interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }

    public static NetworkManager getInstance() {
        if (instance == null)
            instance = new NetworkManager();
        return instance;
    }

    public GitHubApi getGitApi() {
        return retrofit.create(GitHubApi.class);
    }

    public GitHubApi getUserTokenApi() {
        return new Retrofit.Builder()
                .baseUrl("https://github.com")
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
                .create(GitHubApi.class);
    }
}

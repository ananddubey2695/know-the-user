package com.example.gitlist.network;

import com.example.gitlist.core.Core;
import com.google.gson.annotations.SerializedName;

/**
 * Created by anand on 2019-06-23 at 18:12.
 */
public class AuthTokenRequest extends Core {

    @SerializedName("client_id")
    String clientId = "f87eb7b08385bc080150";

    @SerializedName("client_secret")
    String clientSecret = "ad1a806dad847fe7b18340709603d2cc0a82ab31";

    private String code;

    public AuthTokenRequest(String code) {
        this.code = code;
    }
}

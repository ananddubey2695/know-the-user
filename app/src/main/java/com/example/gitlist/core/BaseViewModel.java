package com.example.gitlist.core;

import android.content.Context;

import androidx.lifecycle.ViewModel;

/**
 * Created by anand on 2019-06-23 at 17:34.
 */
public class BaseViewModel extends ViewModel {
    private Context context;

    public BaseViewModel(Context context) {
        this.context = context;
    }
}

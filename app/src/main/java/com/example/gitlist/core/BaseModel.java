package com.example.gitlist.core;

import android.content.Context;

import com.example.gitlist.database.DatabaseManager;
import com.example.gitlist.network.NetworkManager;

/**
 * Created by ananddubey
 * Dated- 2019-06-19
 */
public class BaseModel {
    protected NetworkManager networkManager;
    protected DatabaseManager databaseManager;

    public BaseModel(Context context) {
        networkManager = NetworkManager.getInstance();
        databaseManager = DatabaseManager.getInstance(context);
    }
}

package com.example.gitlist.core;

import com.google.gson.Gson;

import java.io.Serializable;

public abstract class Core implements Serializable {
    @Override
    public String toString() {
        Gson g = new Gson();
        return g.toJson(this);
    }
}
